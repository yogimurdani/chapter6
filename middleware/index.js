class Middleware {
    static putMiddleware(req, res, next) {
      console.log("===Middleware===")
      console.log(req.body)
      const name = req.body.name
      next()
    }
  
    static errorHandler(err, req, res, next) {
      console.log("===Check Error===")
      console.log(err)
      if (err.status) {
        const _status = err.status
        res.status(_status).json(err)
      }
      else {
        res.status(400).json({
          messages: "Ooppsss"
        })
      }
    }
}
  module.exports = Middleware