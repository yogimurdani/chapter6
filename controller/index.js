const Home = require("./home")
const Games = require("./games")
const Login = require("./login")
const Register = require("./register")
const dashboard = require("./dashboard")
const dashboardUser = require("./dashboard-user")
const api = require("./api")

module.exports = {
  Home, Games, api, Login, Register, dashboard, dashboardUser
}