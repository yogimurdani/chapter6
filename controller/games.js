const express = require("express");
const app = express.Router();

app.get("/games", (req, res, next) =>
  res.render("games", {
    title: "Play Now",
    name: req.query.user,
  })
);

module.exports = app;