const express = require("express");
const app = express.Router();

app.get("/", (req, res) =>
  res.render("index", {
    title: "Batu Gunting Kertas",
    name: req.query.user,
  })
);

module.exports = app;
