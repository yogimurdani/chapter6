const express = require("express");
const app = express();
const {Home, Games, api, Login, Register, dashboard, dashboardUser} = require("../controller")
const Middleware = require("../middleware")

app.use(Middleware.putMiddleware, Home, Games, api, Login, Register, dashboard, dashboardUser)

module.exports = app;