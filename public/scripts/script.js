class Player {
  constructor() {
      this.batu = document.getElementsByClassName("batu");
      this.kertas = document.getElementsByClassName("kertas");
      this.gunting = document.getElementsByClassName("gunting");
  }
}

const Computer = (Base) =>
  class extends Base {
      randomPick = (max) => Math.floor(Math.random() * max);
  };


class Player_1 extends Player {
  constructor(batu, kertas, gunting) {
      super(batu, kertas, gunting);
      this.#initiation();
  }


  #initiation() {
      this.batu[0].id = "batu-player";
      this.kertas[0].id = "kertas-player";
      this.gunting[0].id = "gunting-player";
  }
}

class Player_2 extends Computer(Player) {
  constructor(batu, kertas, gunting) {
      super(batu, kertas, gunting);
      this.#initiation();
  }

  #initiation() {
      this.batu[1].id = "batu-com";
      this.kertas[1].id = "kertas-com";
      this.gunting[1].id = "gunting-com";
  }
}

class Rules {
  constructor() {
      this.winBox = document.getElementById("box");
      this.inFo = document.getElementById("h1")
      this.user_choice;
      this.com_choice;
  }

  logger = (text) => {
      console.log("----------");
      console.log(text);
  };

  _defaultState = () => {
      this.winBox.classList.remove("winBox");
      this.winBox.classList.remove("drawBox");
      this.winBox.classList.remove("versus_result");
      this.inFo.innerText = "VS";
      this.inFo.setAttribute("style", "font-size:144px; color:#BD0000; margin:auto")
      this.winBox.appendChild(this.inFo);
  };

  _winResult = () => {
      this.winBox.classList.remove("drawBox");
      this.winBox.classList.add("versus_result");
      this.winBox.classList.add("winBox");
      this.inFo.innerText = "PLAYER 1 WIN";
      this.inFo.setAttribute("style", "font-size:38px; color:white; margin:auto");
      this.winBox.appendChild(this.inFo);
      this.logger("Result : PLAYER 1 Win)");
  };

  _loseResult = () => {
      this.winBox.classList.remove("drawBox");
      this.winBox.classList.add("versus_result");
      this.winBox.classList.add("winBox");
      this.inFo.innerText = "COM WIN";
      this.inFo.setAttribute("style", "font-size:38px; color:white; margin:auto");
      this.winBox.appendChild(this.inFo);
      this.logger("Result : COM Win(");
  };

  _drawResult = () => {
      this.winBox.classList.add("versus_result");
      this.winBox.classList.add("drawBox");
      this.inFo.innerText = "DRAW";
      this.inFo.setAttribute("style", "font-size:38px; color:white; margin:auto");
      this.winBox.appendChild(this.inFo);
      this.logger("Result : Draw");
  };

  decision = (userChoice, botChoice) => {
      if (
          (userChoice === "batu" && botChoice === "batu") ||
          (userChoice === "kertas" && botChoice === "kertas") ||
          (userChoice === "gunting" && botChoice === "gunting")
      ) {
          return this._drawResult();
      } else if (
          (userChoice === "batu" && botChoice === "gunting") ||
          (userChoice === "kertas" && botChoice === "batu") ||
          (userChoice === "gunting" && botChoice === "kertas")
      ) {
          return this._winResult();
      } else if (
          (userChoice === "batu" && botChoice === "kertas") ||
          (userChoice === "kertas" && botChoice === "gunting") ||
          (userChoice === "gunting" && botChoice === "batu")
      ) {
          return this._loseResult();
      }
  };
}

class Game extends Rules {
  constructor(user_choice, com_choice) {
      super(user_choice, com_choice);
      this.resetResult = document.getElementById("reset");
      this.#initiation();
  }

  #initiation() {
      this.user = new Player_1();
      this.com = new Player_2();
      this._defaultState();
      this.resetButton();
  }

  getUserPick = (choice) => {
      this.user_choice = choice;
      this.logger(`Player choose: ${this.user_choice}`);
      return this.user_choice;
  };

  getComPick = (choice) => {
      this.com_choice = choice;
      this.logger(`Com choose: ${this.com_choice}`);
      return this.com_choice;
  };

  setPlayerListener = () => {
      this.user.batu[0].onclick = () => {
          this.getUserPick("batu");
          this.user.batu[0].classList.add("chosen");
          this.user.kertas[0].classList.remove("chosen");
          this.user.gunting[0].classList.remove("chosen");
          this.removePlayerListener();
          this.decideResult();
      };

      this.user.kertas[0].onclick = () => {
          this.getUserPick("kertas");
          this.user.batu[0].classList.remove("chosen");
          this.user.kertas[0].classList.add("chosen");
          this.user.gunting[0].classList.remove("chosen");
          this.removePlayerListener();
          this.decideResult();
      };

      this.user.gunting[0].onclick = () => {
          this.getUserPick("gunting");
          this.user.batu[0].classList.remove("chosen");
          this.user.kertas[0].classList.remove("chosen");
          this.user.gunting[0].classList.add("chosen");
          this.removePlayerListener();
          this.decideResult();
      };
  };

  setComListener(choice) {
      switch (choice) {
          case "batu":
              this.getComPick("batu");
              this.com.batu[1].classList.add("chosen");
              this.com.kertas[1].classList.remove("chosen");
              this.com.gunting[1].classList.remove("chosen");
              break;
          case "kertas":
              this.getComPick("kertas");
              this.com.batu[1].classList.remove("chosen");
              this.com.kertas[1].classList.add("chosen");
              this.com.gunting[1].classList.remove("chosen");
              break;
          case "gunting":
              this.getComPick("gunting");
              this.com.batu[1].classList.remove("chosen");
              this.com.kertas[1].classList.remove("chosen");
              this.com.gunting[1].classList.add("chosen");
              break;
          default:
              break;
      }
  }

  removePlayerListener = () => {
      document.getElementsByClassName("batu")[0].disabled = true;
      document.getElementsByClassName("kertas")[0].disabled = true;
      document.getElementsByClassName("gunting")[0].disabled = true;
  };

  result = () => {
      setInterval(() => {
          if (this.user_choice && this.com_choice) {
              this.decision(this.user_choice, this.com_choice);
          }
          this.user_choice = null;
          this.com_choice = null;
      }, 400);
  };

  decideResult() {
      switch (this.com.randomPick(3)) {
          case 2:
              this.setComListener("batu");
              this.result();
              break;
          case 1:
              this.setComListener("kertas");
              this.result();
              break;
          case 0:
              this.setComListener("gunting");
              this.result();
              break;
          default:
              break;
      }
  }

  resetButton() {
      this.resetResult.onclick = () => {
          this.logger("Game restarted !");
          this._defaultState();
          document.querySelectorAll(".choice").forEach((userButton) => {
              userButton.classList.remove("chosen");
              userButton.disabled = false;
          });
      };
  }

  play() {
      this.logger("Mainkan");
      this.setPlayerListener();
  }
}

const game = new Game();
game.play();
